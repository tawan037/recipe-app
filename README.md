# recipe-app



## Getting started

** config
1. checkout branch ไป origin/master หลังจากนั้นให้ Trust project 
2. run app

## Integrate with your tools
 
 * retrofit
 * room database
 * lifecycle
 * picasso


## Name
recipe-app

## Description
Use recipes.json to build an application that shows a list of recipes via HTTP request.
When the app is first launched, fetch the recipes JSON from the API and store it in the app DB.


